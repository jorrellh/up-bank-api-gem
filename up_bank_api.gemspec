Gem::Specification.new do |s|
  s.name      = 'Up Bank API'
  s.version   = '0.0.0'
  s.summary   = 'Up Bank API allowing bank members to read their account information'
  s.authors   = ['Jorrell Holtze']
  s.email     = 'jorrell@sansserif.club'
  s.homepage  = 'https://gitlab.com/jorrellh/up-bank-api-gem/'
  s.license   = 'MIT'
end
