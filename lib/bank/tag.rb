require 'hash_dot'
require 'dotenv/load'

class Tag

    Hash.use_dot_syntax = true

    attr_reader :attributes, :type, :id

    def initialize(json)
        @type = json["type"]
        @id = json["id"]

        @relationships = {
            transactions: {
                links: {
                    related: json["relationships"]["transactions"]["links"]["related"]
                }
            }
        }

    end

end