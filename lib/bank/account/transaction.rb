require 'hash_dot'
require './lib/bank/category'

class Transaction

    Hash.use_dot_syntax = true

    attr_reader :type, :id, :attributes, :relationships, :links

    def initialize(json)
        @type = json["type"]
        @id = json["id"]
        @attributes = {
            status: json["attributes"]["status"],
            raw_text: json["attributes"]["rawText"],
            description: json["attributes"]["description"],
            message: json["attributes"]["message"],
            hold_info: {
                amount: {
                    currency_code: nil, 
                    value: nil, 
                    value_in_base_units: nil
                },
                foreign_amount: nil, 
            },
            round_up: {
                amount: {
                    currency_code: nil, 
                    value: nil,
                    value_in_base_units: nil
                },
            },
            cashback: json["attributes"]["cashback"],
            amount: {
                currency_code: json["attributes"]["amount"]["currencyCode"],
                value: json["attributes"]["amount"]["value"],
                value_in_base_units: json["attributes"]["amount"]["valueInBaseUnits"]
            },
            foreign_amount: {
                currency_code: nil, 
                value: nil,
                value_in_base_units: nil
            },
            settled_at: json["attributes"]["settledAt"],
            created_at: json["attributes"]["createdAt"]
        }

        if !json["attributes"]["holdInfo"].nil?
            @attributes.hold_info.amount.currency_code = json["attributes"]["holdInfo"]["amount"]["currencyCode"],
            @attributes.hold_info.amount.value = json["attributes"]["holdInfo"]["amount"]["value"],
            @attributes.hold_info.amount.value_in_base_units = json["attributes"]["holdInfo"]["amount"]["valueInBaseUnits"]
        end

        if !json["attributes"]["roundUp"].nil?
            @attributes.round_up.amount.currency_code = json["attributes"]["roundUp"]["amount"]["currencyCode"],
            @attributes.round_up.amount.value = json["attributes"]["roundUp"]["amount"]["value"],
            @attributes.round_up.amount.value_in_base_units = json["attributes"]["roundUp"]["amount"]["valueInBaseUnits"]
        end

        if !json["attributes"]["foreignAmount"].nil?
            @attributes.foreign_amount.currency_code = json["attributes"]["foreignAmount"]["amount"]["currencyCode"],
            @attributes.foreign_amount.value = json["attributes"]["foreignAmount"]["amount"]["value"],
            @attributes.foreign_amount.alue_in_base_units = json["attributes"]["foreignAmount"]["amount"]["valueInBaseUnits"]
        end

        @relationships = {
            account: {
                data: {
                    type: json["relationships"]["account"]["data"]["type"],
                    id: json["relationships"]["account"]["data"]["id"]
                },
                links: {
                    related: json["relationships"]["account"]["links"]["related"]
                }
            },
            transfer_account: {
                # TODO Complete this nested hash
                data: json["relationships"]["transferAccount"]["data"]
            },
            category: {
                # TODO Complete this nested hash
                data: json["relationships"]["category"]["data"]
            },
            parent_category: {
                # TODO Complete this nested hash
                data: json["relationships"]["parentCategory"]["data"]
            },
            tags: {
                data: json["relationships"]["tags"]["data"],
                links: {
                    self: json["relationships"]["tags"]["links"]["self"]
                }
            }
        }
        
        @links = {
            self: json["links"]["self"]
        }
        

    end

end