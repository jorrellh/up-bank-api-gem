require 'hash_dot'
require 'dotenv/load'

class Category

    Hash.use_dot_syntax = true

    attr_reader :attributes, :type, :relationships, :links, :id

    def initialize(json)
        @type = json["type"]
        @id = json["id"]


        @attributes = { 
            name: json["attributes"]["name"]
        }
        @relationships = {

            # (1) We nullify these keys because they can be nil and throw an error
            parent: {
                data: {
                    type: nil,
                    id: nil
                },
                links: {
                    related: nil
                }
            },
            # if these throw a nil error, repeat what was done with parent.
            children: {
                data: json["relationships"]["children"]["data"],
                links: {
                    related: json["relationships"]["children"]["links"]["related"]
                } 
            }
        }

        # (2) However, if the data exists, we append them manually
        if !json["relationships"]["parent"]["data"].nil?
            @relationships.parent.data.type = json["relationships"]["parent"]["data"]["type"]
            @relationships.parent.data.id = json["relationships"]["parent"]["data"]["id"]
            @relationships.parent.links.related = json["relationships"]["parent"]["links"]["related"]
        end

        @links = {
            self: json["links"]["self"]
        }
    end

end