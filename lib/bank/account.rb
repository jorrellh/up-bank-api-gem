require 'hash_dot'
require 'dotenv/load'
require './lib/bank/account/transaction'

class Account

    Hash.use_dot_syntax = true

    attr_reader :attributes, :type, :relationships, :links, :transactions, :id

    def initialize(json)
        @type = json["type"]
        @id = json["id"]

        transactions_url = "https://api.up.com.au/api/v1/accounts/#{@id}/transactions"
        transactions_json = JSON.parse(RestClient.get transactions_url, {:Authorization => 'Bearer ' + ENV['ACCESS_TOKEN']})

        @transactions = []

        transactions_json["data"].each do |t|
            @transactions << Transaction.new(t)
        end

        @attributes = { 
            display_name: json["attributes"]["displayName"],
            account_type: json["attributes"]["accountType"],
            balance: {
                currency_code: json["attributes"]["balance"]["currencyCode"],
                value: json["attributes"]["balance"]["value"],
                value_in_base_units: json["attributes"]["balance"]["valueInBaseUnits"]
            },
            created_at: json["attributes"]["createdAt"]
        }
        @relationships = {
            transactions: {
                links: {
                    related: json["relationships"]["transactions"]["links"]["related"]
                }
            }
        }

        @links = {
            self: json["links"]["self"]
        }
    end



    

end