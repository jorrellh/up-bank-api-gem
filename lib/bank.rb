require 'rest-client'
require 'json'
require 'dotenv/load'

require './lib/bank/account'
require './lib/bank/category'
require './lib/bank/tag'

class Bank

  attr_reader :accounts, :categories, :tags

  def initialize
    account_url = "https://api.up.com.au/api/v1/accounts"
    account_json = JSON.parse(RestClient.get account_url, {:Authorization => 'Bearer ' + ENV['ACCESS_TOKEN']})

    @accounts = []
    account_json["data"].each do |a|
      @accounts << Account.new(a)
    end

    categories_url = "https://api.up.com.au/api/v1/categories"
    categories_json = JSON.parse(RestClient.get categories_url, {:Authorization => 'Bearer ' + ENV['ACCESS_TOKEN']})

    @categories = []
    categories_json["data"].each do |t|
        @categories << Category.new(t)
    end

    tags_url = "https://api.up.com.au/api/v1/tags"
    tags_json = JSON.parse(RestClient.get tags_url, {:Authorization => 'Bearer ' + ENV['ACCESS_TOKEN']})

    @tags = []
    tags_json["data"].each do |t|
        @tags << Tag.new(t)
    end


  end
  

end
