# Up Bank API Ruby Wrapper

This ruby application requests banking information for a given access token. 

Place your access token in the `.env` file. There is an example file.

It can request the API for all of your accounts and provide all of the transactions for each one including the cateogories and tags associated with them.

Further plans to turn this into a proper installable ruby gem at some stage for CLI usage.


Documentation used to make this: [https://developer.up.com.au/](https://developer.up.com.au/)
